import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import ShipmentLog from "./components/ShipmentLog";
import "./App.css";

function App() {
  return (
    <div>
      <Container fluid>
        <ShipmentLog />
      </Container>
    </div>
  );
}

export default App;
