import React from "react";
import OriginMarker from "../origin.png";
import DestMarker from "../dest.png";

// A map component for shipments that shows the route between the origin and destination
class ShipmentMap extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      google: null,
      mapLoaded: window.mapLoaded,
      map: null,
      id: "",
      markers: [],
      directionsRenderer: null,
    };

    this.updateMap = this.updateMap.bind(this);
  }

  // This function handles clearing previous markers/routes and displaying new ones
  updateMap = (selectedShipmentID, selectedOrigin, selectedDestination) => {
    // Clear out previous markers
    var i = 0;
    for (i = 0; i < this.state.markers.length; i++) {
      this.state.markers[i].setMap(null);
    }

    // Clear out previous routes
    if (this.state.directionsRenderer !== null) {
      this.state.directionsRenderer.setMap(null);
    }

    // Make sure we are displaying a different shipment before updating the map
    if (
      this.state.google !== null &&
      selectedShipmentID !== "" &&
      selectedShipmentID !== this.state.id &&
      this.state.id !== "-1" &&
      (selectedOrigin !== "") & (selectedDestination !== "")
    ) {
      var newDirectionsRenderer = null;
      var newMarkers = [];

      // Create a route based on the selected origin and destination
      const directionsService = new this.state.google.maps.DirectionsService();
      directionsService.route(
        {
          origin: selectedOrigin,
          destination: selectedDestination,
          travelMode: this.state.google.maps.TravelMode.DRIVING,
        },
        (response, status) => {
          if (status === "OK") {
            // Display the route
            newDirectionsRenderer = new this.state.google.maps.DirectionsRenderer({
              map: this.state.map,
              directions: response,
              suppressMarkers: true,
            });

            // Display the origin marker
            var leg = response.routes[0].legs[0];
            newMarkers.push(
              new this.state.google.maps.Marker({
                position: leg.start_location,
                map: this.state.map,
                icon: OriginMarker,
                title: "Origin",
              })
            );

            // Display the destination marker
            newMarkers.push(
              new this.state.google.maps.Marker({
                position: leg.end_location,
                map: this.state.map,
                icon: DestMarker,
                title: "Destination",
              })
            );

            // Update the state with the new route and markers
            this.setState({ directionsRenderer: newDirectionsRenderer, markers: newMarkers });
          } else {
            console.log(status);
          }
        }
      );
    }
  };

  render() {
    return <div id="shipmentMapRoot"></div>;
  }

  // Initialize the google map after the document has been loaded
  componentDidMount() {
    document.onreadystatechange = () => {
      if (document.readyState === "complete") {
        const google = window.google;
        this.setState({
          google: google,
          mapLoaded: true,
          map: new google.maps.Map(document.getElementById("shipmentMapRoot"), {
            center: { lat: 39, lng: -98 },
            zoom: 5,
            disableDefaultUI: true,
          }),
        });
      }
    };
  }
}

export default ShipmentMap;
