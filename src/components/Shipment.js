import React from "react";
import Button from "react-bootstrap/Button";

// A component that represents a single shipment which is stored in the postgreSQL database
class Shipment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipmentID: props.shipment[0],
      shipperName: props.shipment[1],
      carrierName: props.shipment[2],
      originAddress: props.shipment[3],
      destinationAddress: props.shipment[4],
    };

    this.selectShipmentToDisplay = this.selectShipmentToDisplay.bind(this);
    this.deleteShipment = this.deleteShipment.bind(this);
  }

  // Passes the selected shipment back up through the ShipmentList component to the ShipmentLog component
  selectShipmentToDisplay = () => {
    this.props.selectShipment(this.state.shipmentID, this.state.originAddress, this.state.destinationAddress);
  };

  // Passes the shipment ID of the shipment that will be deleted to the ShipmentList component
  deleteShipment = () => {
    this.props.deleteShipment(this.state.shipmentID);
  };

  render() {
    return (
      <tr className={"rowSelected " + (this.props.selectedShipmentID === this.state.shipmentID ? "show" : "hidden")}>
        <td onClick={() => this.selectShipmentToDisplay()}>{this.state.shipmentID}</td>
        <td onClick={() => this.selectShipmentToDisplay()}>{this.state.shipperName}</td>
        <td onClick={() => this.selectShipmentToDisplay()}>{this.state.carrierName}</td>
        <td onClick={() => this.selectShipmentToDisplay()}>{this.state.originAddress}</td>
        <td onClick={() => this.selectShipmentToDisplay()}>{this.state.destinationAddress}</td>
        <td>
          <Button variant="danger" onClick={() => this.deleteShipment()}>
            Delete
          </Button>
        </td>
      </tr>
    );
  }
}

export default Shipment;
