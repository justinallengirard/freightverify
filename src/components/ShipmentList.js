import React from "react";
import Shipment from "./Shipment";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";

// A component that represents a list of Shipment components
class ShipmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      shipments: [],
    };

    this.deleteShipment = this.deleteShipment.bind(this);
    this.updateShipments = this.updateShipments.bind(this);
  }

  // Returns the AWS API Gateway URL for making changes to the postgreSQL database
  getURLPath = () => {
    return "https://2bbtvbzcye.execute-api.us-east-2.amazonaws.com/dev/";
  };

  // Returns the headers for the AWS API Gateway calls
  getHeaders = () => {
    return {
      "Content-Type": "application/json",
    };
  };

  // Given a shipment ID, this function will find its location in that state's shipments array
  getShipmentIndex = (shipmentID) => {
    var i = 0;
    for (i = 0; i < this.state.shipments.length; i++) {
      if (this.state.shipments[i][0] === shipmentID) {
        return i;
      }
    }
  };

  // Updates the state shipments array with the current list of shipments
  updateShipments = () => {
    var url = new URL(this.getURLPath());
    url.searchParams.append("shipment_id", "-1");

    this.props.handleFetching(true);
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: "omit",
      body: null,
      headers: this.getHeaders(),
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({ shipments: result });
          this.props.handleFetching(false);
        },
        (error) => {
          console.log(error);
          this.props.handleFetching(false);
        }
      );
  };

  // Deletes a shipment from the postgreSQL database
  deleteShipment = (shipmentID) => {
    var url = new URL(this.getURLPath());
    url.searchParams.append("shipment_id", shipmentID);
    this.props.handleFetching(true);
    fetch(url, {
      method: "DELETE",
      mode: "cors",
      credentials: "omit",
      body: null,
      headers: this.getHeaders(),
    })
      .then((res) => res.json())
      .then(
        (result) => {
          this.props.updateShipments();
        },
        (error) => {
          console.log(error);
          this.props.handleFetching(false);
        }
      );
  };

  // Retrieves all shipments from the postgreSQL database
  componentDidMount() {
    this.updateShipments();
  }

  render() {
    if (this.state.isFetching) {
      return (
        <div id="spinnerContainer">
          <Spinner id="spinner" animation="border" variant="primary" />
        </div>
      );
    } else {
      return (
        <div id="tableContainer">
          <div id="flexTable">
            <Table bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Shipper</th>
                  <th>Carrier</th>
                  <th>Origin</th>
                  <th>Destination</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.shipments.map((shipment) => (
                  <Shipment
                    key={shipment[0]}
                    shipment={shipment}
                    selectShipment={this.props.selectShipment}
                    deleteShipment={this.deleteShipment}
                    selectedShipmentID={this.props.selectedShipmentID}
                  />
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      );
    }
  }

  // Handles setting the component's fetch state
  static getDerivedStateFromProps(props, state) {
    if (props.isFetching !== state.isFetching) {
      return { isFetching: props.isFetching };
    }

    return {};
  }
}

export default ShipmentList;
