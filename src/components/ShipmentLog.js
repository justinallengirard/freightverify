import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ShipmentForm from "./ShipmentForm";
import ShipmentList from "./ShipmentList";
import ShipmentMap from "./ShipmentMap";
import Truck from "../truck.png";

// The highest order component that represents the shipment log application
class ShipmentLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      selectedShipmentID: -1,
      showAboutModal: false,
    };
    this.selectShipmentToDisplay = this.selectShipmentToDisplay.bind(this);
    this.handleFetching = this.handleFetching.bind(this);
    this.updateShipments = this.updateShipments.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.shipmentList = React.createRef();
    this.shipmentMap = React.createRef();
  }

  // Keeps track if the application is fetching data from the API so that a loading spinner can be displayed
  handleFetching = (isFetching) => {
    this.setState({ isFetching: isFetching });
  };

  // Tells the ShipmentList to update
  updateShipments = () => {
    this.shipmentList.current.updateShipments();
    this.shipmentMap.current.updateMap("-1", "", "");
    this.setState({ selectedShipmentID: "-1" });
  };

  // Tells the map to display the selected shipment
  selectShipmentToDisplay = (selectedShipmentID, selectedOriginAddress, selectedDestinationAddress) => {
    this.shipmentMap.current.updateMap(selectedShipmentID, selectedOriginAddress, selectedDestinationAddress);
    this.setState({ selectedShipmentID: selectedShipmentID });
  };

  // Handles toggling the About modal
  toggleModal = () => {
    this.setState({ showAboutModal: !this.state.showAboutModal });
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>
            <img alt="truck" src={Truck} width="30" height="30" className="d-inline-block align-top" /> Shipment Log
          </Navbar.Brand>
          <Nav>
            <Nav.Item onClick={() => this.toggleModal()}>About</Nav.Item>
          </Nav>
        </Navbar>
        <Row>
          <Col sm={6} id="shipmentManagement">
            <Row id="formRow">
              <ShipmentForm isFetching={this.state.isFetching} handleFetching={this.handleFetching} updateShipments={this.updateShipments} />
            </Row>
            <Row id="listRow">
              <ShipmentList
                ref={this.shipmentList}
                isFetching={this.state.isFetching}
                handleFetching={this.handleFetching}
                selectedShipmentID={this.state.selectedShipmentID}
                selectShipment={this.selectShipmentToDisplay}
                updateShipments={this.updateShipments}
              />
            </Row>
          </Col>
          <Col sm={6} id="shipmentMapCol">
            <ShipmentMap ref={this.shipmentMap} />
          </Col>
        </Row>
        <Modal centered show={this.state.showAboutModal} onHide={this.toggleModal}>
          <Modal.Header closeButton>
            <Modal.Title>About</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>This application was developed for FreightVerify by myself, Justin Girard, to demonstate my ability in learning a new tech stack.</p>
            <p>
              <b>Email: </b>JustinAllenGirard@gmail.com
            </p>
            <b>
              <u>Developed Using:</u>
            </b>
            <ul>
              <li>React</li>
              <li>React-Bootstrap</li>
              <li>Python</li>
              <li>PostgreSQL</li>
              <li>Google Maps API</li>
              <li>VSCode</li>
              <li>GitLab</li>
            </ul>
            <b>
              <u>AWS Services Used:</u>
            </b>
            <ul>
              <li>Route 53</li>
              <li>Amplify</li>
              <li>API Gateway</li>
              <li>Lambda</li>
              <li>RDS</li>
            </ul>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.toggleModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ShipmentLog;
