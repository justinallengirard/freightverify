import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

// A form component to add Shipments
class ShipmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      origin: null,
      destination: null,
      formShipper: "",
      formCarrier: "",
      formOriginAddress: "",
      formDestinationAddress: "",
      isFetching: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleOriginSelect = this.handleOriginSelect.bind(this);
    this.handleDestinationSelect = this.handleDestinationSelect.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }

  // Handles updating the state everytime an input field is updated
  handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value,
    });
  };

  // Handles updating the state of the origin address when an AutoCompleted Google address is selected
  handleOriginSelect = () => {
    var originPlace = this.state.origin.getPlace();
    var address = originPlace.formatted_address;
    this.setState({
      formOriginAddress: address,
    });
  };

  // Handles updating the state of the destination address when an AutoCompleted Google address is selected
  handleDestinationSelect = () => {
    var destinationPlace = this.state.destination.getPlace();
    var address = destinationPlace.formatted_address;
    this.setState({
      formDestinationAddress: address,
    });
  };

  // Adds the shipment to the postgreSQL database
  formSubmit = (event) => {
    event.preventDefault();

    // Validate that the given addresses are good
    var originPlace = this.state.origin.getPlace();
    var destinationPlace = this.state.destination.getPlace();
    if (!originPlace || !originPlace.geometry) {
      alert("Please enter a valid origin address");
    } else if (!destinationPlace || !destinationPlace.geometry) {
      alert("Please enter a valid destination address");
    } else if (this.state.formOriginAddress.trim() === this.state.formDestinationAddress.trim()) {
      alert("The origin address and destination address cannot be the same");
    } else {
      // The url and parameters for the AWS API Gateway call
      var url = new URL("https://2bbtvbzcye.execute-api.us-east-2.amazonaws.com/dev/");
      url.searchParams.append("shipper_name", this.state.formShipper.trim());
      url.searchParams.append("carrier_name", this.state.formCarrier.trim());
      url.searchParams.append("origin_address", this.state.formOriginAddress.trim());
      url.searchParams.append("destination_address", this.state.formDestinationAddress.trim());

      // Headers for the AWS API Gateway call
      let headers = {
        "Content-Type": "application/json",
      };

      this.props.handleFetching(true);
      fetch(url, {
        method: "POST",
        mode: "cors",
        credentials: "omit",
        body: null,
        headers: headers,
      })
        .then((res) => res.json())
        .then(
          (result) => {
            this.setState({ formShipper: "", formCarrier: "", formOriginAddress: "", formDestinationAddress: "" });
            this.props.updateShipments();
            this.props.handleFetching(false);
          },
          (error) => {
            console.log(error);
            this.setState({ formShipper: "", formCarrier: "", formOriginAddress: "", formDestinationAddress: "" });
            this.props.handleFetching(false);
          }
        );
    }
  };

  render() {
    return (
      <div id="formContainer">
        <Form onSubmit={this.formSubmit}>
          <Form.Row>
            <Form.Group controlId="formShipperName">
              <Form.Label>Shipper</Form.Label>
              <Form.Control
                name="formShipper"
                type="text"
                placeholder="Enter the shipper's name..."
                value={this.state.formShipper}
                onChange={this.handleInputChange}
                required
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group controlId="formCarrierName">
              <Form.Label>Carrier</Form.Label>
              <Form.Control
                name="formCarrier"
                type="text"
                placeholder="Enter the carrier's name..."
                value={this.state.formCarrier}
                onChange={this.handleInputChange}
                required
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group controlId="formOrigin">
              <Form.Label>Origin</Form.Label>
              <Form.Control
                name="formOriginAddress"
                type="text"
                placeholder="Enter the origin address for the shipment..."
                value={this.state.formOriginAddress}
                onChange={this.handleInputChange}
                required
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Form.Group controlId="formDestination">
              <Form.Label>Destination</Form.Label>
              <Form.Control
                name="formDestinationAddress"
                type="text"
                placeholder="Enter the destination address for the shipment..."
                value={this.state.formDestinationAddress}
                onChange={this.handleInputChange}
                required
              />
            </Form.Group>
          </Form.Row>
          <Form.Row>
            <Button variant="primary" type="submit">
              Add Shipment
            </Button>
          </Form.Row>
        </Form>
      </div>
    );
  }

  // Initializes the Google AutoComplete address fields once the document is loaded
  componentDidMount() {
    document.addEventListener("readystatechange", (event) => {
      if (event.target.readyState === "complete") {
        const google = window.google;
        var options = {
          componentRestrictions: { country: "us" },
        };
        const originAutoComplete = new google.maps.places.Autocomplete(document.getElementById("formOrigin"), options);
        originAutoComplete.addListener("place_changed", this.handleOriginSelect);
        const destinationAutoComplete = new google.maps.places.Autocomplete(document.getElementById("formDestination"), options);
        destinationAutoComplete.addListener("place_changed", this.handleDestinationSelect);
        this.setState({ origin: originAutoComplete, destination: destinationAutoComplete });
      }
    });
  }

  // Handles setting the component's fetch state
  static getDerivedStateFromProps(props, state) {
    if (props.isFetching !== state.isFetching) {
      return { isFetching: props.isFetching };
    }

    return {};
  }
}

export default ShipmentForm;
